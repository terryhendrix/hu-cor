import sbt.Keys._

organization := "nl.bluesoft.tryouts"

name := "chain.of.responsibilities"

version := "1.1.0-SNAPSHOT"

scalaVersion := "2.11.2"

resolvers += "Local Maven Repository" at "file://"+Path.userHome.absolutePath+"/.m2/repository"

resolvers += "krasserm at bintray" at "http://dl.bintray.com/krasserm/maven"

credentials += Credentials(Path.userHome / ".ivy2" / ".credentials")

libraryDependencies ++=
  {
    Seq(
      "org.scala-lang"           % "scala-library"                  % scalaVersion.value
    )
  }

autoCompilerPlugins := true

scalacOptions := Seq("-unchecked", "-deprecation", "-encoding", "utf8")

publishMavenStyle := true

publishArtifact in Test := false

fork := true

fork in run := true

fork in test := true
