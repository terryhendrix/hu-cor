package nl.bluesoft.tryout.cor
import scala.util.Try
import java.util.{UUID, Date}

/**
 * Created by terryhendrix on 22/02/15.
 */
case class Product(productNr:String, name:String, description:String, price:Double)
case class OrderedProduct(product:Product, amount:Int) {
  def total = amount * product.price
}
case class Order(orderId:UUID, products:List[OrderedProduct], payed:Boolean) {
  def total = products.foldLeft(0.0){ (a, b) => a + b.total}
}

abstract class Handler[U] {
  /** The next handler in the chain. */
  val successor:Option[Handler[_]] = None

  def handleRequest(message:Any):Unit = handleRequest(message, 0)

  /** Passes the message */
  private def handleRequest(message:Any, accumulator:Int):Unit = {
    Try(handle(message.asInstanceOf[U])).map { _ =>
      forward(message, accumulator+1)
    }.recover{ case _ =>
      forward(message, accumulator)
    }
  }

  /** abstract method, to be implemented in child-classes */
  protected def handle(message:U)

  def forward(message:Any, acc:Int) = {
    successor match {
      case Some(next) =>
        next.handleRequest(message, acc)
      case None if acc > 0 =>
        println(s"The request was handled $acc times\n")
      case None =>
        println(s"Warning: Unhandled message $message\n")
    }
  }
}

class Shipping(override val successor:Option[Handler[_]] = None) extends Handler[Order] {
  override protected def handle(message: Order) = {
    message match {
      case order:Order if order.payed=>
        println(s"\nShipping order ${order.orderId} ")
        order.products.foreach(pr => println(s" - ${pr.amount} x ${pr.product.name}"))
    }
  }
}

class Registration(override val successor:Option[Handler[_]] = None) extends Handler[Order]{
  override protected def handle(message: Order) = {
    message match {
      case order:Order if !order.payed =>
        println(s"\nRegistering order ${order.orderId}")
        order.products.foreach(pr => println(s" - ${pr.amount} x ${String.format("%-15s",pr.product.name)} = $$${pr.total}"))
        println(s"Order total order ${order.total}")
      case order:Order if order.payed =>
        println(s"\nRegistering that order ${order.orderId} is payed")
    }
  }
}

class Payment(override val successor:Option[Handler[_]] = None) extends Handler[Order] {
  override protected def handle(message: Order) = {
    message match {
      case order:Order if !order.payed =>
        println(s"\nHandling ideal payment for order ${order.orderId} with total $$${order.total}")
    }
  }
}


object Cor extends App {
  implicit def handlerToOption[U](handler:Handler[U]) = Some(handler)
  val payment = new Payment()
  val shipping = new Shipping(payment)
  val registration = new Registration(shipping)

  val laptop = Product(productNr = "1", name = "Laptop", description = "Lenovo", price = 600)
  val mouse = Product(productNr = "1", name = "Mouse", description = "Microsoft", price = 21)
  val screenWiper = Product(productNr = "10", name = "Screen wiper", description = "Box of 5 pieces", price = 5)
  val order = Order(
    orderId = UUID.randomUUID(),
    products = List(
      OrderedProduct(laptop, 1),
      OrderedProduct(mouse, 1),
      OrderedProduct(screenWiper, 5)
    ),
    payed = false
  )

  println("================================")
  println("====== Handling new order ======")
  println("================================")
  registration.handleRequest(order)
  val payedOrder = order.copy(payed = true)

  println("================================")
  println("===== Handling payed order =====")
  println("================================")
  registration.handleRequest(payedOrder)

  println("================================")
  println("====== Unhandled message ======")
  println("================================")
  registration.handleRequest(laptop)
}
